#!/usr/bin/env python3


## TODO ##
## CREAR UNA SÈRIE DE VARIABLES QUE SIGUIN EMAS, ETC.
## Amb la finalitat de generar alertes sota X condicions.


## Preguntar a xatgpt sobre les bandes de bòlinger. He d'entendre com funciona a nivell de valors numèrics (sense utilitzar cap gràfic) per a afegir la variable a ls generació d'alertes
## Pregunar a xatgpt sobre volum:
##   - Diferents tipus de volum que es poden mesurar amb indicadors tècnics de tradingview
##   - Entendre bé qùe és el volum DELTA i com funciona.
#
#
## Les alertes poden avisar o crear/modificar trades
#
#
## Params analitzar per aletres
##    Quan volum supera la mitjana mobil del volum

import os
import time
import json
import requests
import math
import hmac
from hashlib import sha256
#import pandas as pd
#import numpy as np

def global_consts():
    ## Dades identificació
    with open('berb-api-auth.json', 'r') as file:
        data = json.load(file)
    api_key = data["api_key"]
    secret_key = data["secret_key"]
    return api_key, secret_key

api_key, secret_key = global_consts()

#print("La var api_key val: " + api_key)
#print("La var api_key secret: " + secret_key)

APIURL = "https://open-api.bingx.com";
APIKEY = api_key
SECRETKEY = secret_key

###########################################################
## Funcions universals d'autenticació i consulta a l'API ##
###########################################################

def get_sign(api_secret, payload):
    signature = hmac.new(api_secret.encode("utf-8"), payload.encode("utf-8"), digestmod=sha256).hexdigest()
    print("sign=" + signature)
    return signature

def send_request(method, path, urlpa, payload):
    url = "%s%s?%s&signature=%s" % (APIURL, path, urlpa, get_sign(SECRETKEY, urlpa))
    print(url)
    headers = {
        'X-BX-APIKEY': APIKEY,
    }
    response = requests.request(method, url, headers=headers, data=payload)
    return response.text

def praseParam(paramsMap):
    sortedKeys = sorted(paramsMap)
    paramsStr = "&".join(["%s=%s" % (x, paramsMap[x]) for x in sortedKeys])
    return paramsStr+"&timestamp="+str(int(time.time() * 1000))

######################################
## Funcions Perp consultes generals ##
######################################
## Get all perp contracts
def get_perp_contracts():
    payload = {}
    path = '/openApi/swap/v2/quote/contracts'
    method = "GET"
    paramsMap = {}
    paramsStr = praseParam(paramsMap)
    return send_request(method, path, paramsStr, payload)

## Get last price for a trading pair
def get_perp_pair_last_price(symbol):
    payload = {}
    path = '/openApi/swap/v2/quote/price'
    method = "GET"
    paramsMap = {
    "symbol": symbol,
}
    paramsStr = praseParam(paramsMap)
    resultat_json = send_request(method, path, paramsStr, payload)
    # return resultat
    parsed_data = json.loads(resultat_json)
    resultat = float(parsed_data['data']['price'])
    return resultat


if __name__ == '__main__':
    symbol = "APE-USDT"
    ## Get all perp contracts
    # print("get_perp_contracts:", get_perp_contracts())
    get_perp_pair_last_price(symbol)
    '''
    ultim_preu = get_perp_pair_last_price(symbol)
    print("Preu actual de", symbol, ": ", ultim_preu)
    '''




#########################################
## Mètode PYTHON lib (API) NO UTILITZO ##
#########################################
###from bingX import BingX
###from bingX.perpetual.v2 import PerpetualV2
#bingx_client = PerpetualV2(api_key="api_key", secret_key="secret_key")
## Get symbol perpetual and last price of BTC/USDT
#response = bingx_client.market.get_ticker("BTC-USDT")
#symbol = response["symbol"]
#last_price = response["lastPrice"]
#print("Simbol val: " + symbol)
#print("last_price val: " + last_price)


