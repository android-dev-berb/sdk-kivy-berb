## Imports globals
import os
import time
import json
import requests
import math
import hmac
from hashlib import sha256

# Imports Kivy
import kivy
kivy.require('2.2.1') # replace with your current kivy version !

from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
## from kivy.uix.gridlayout import GridLayout                     

from kivy.config import Config
Config.set("graphics", "width", 400)
Config.set("graphics", "height", 400)

class MainApp(App):
    def build(self):
        dir_path = os.getcwd()
        label = Label(text=f"Directori actual:\n{dir_path}",
                      color=(1, 1, 1, 1),  # Color blanc
                      font_size='20sp',  # Mida de la font
                      halign='center', valign='middle')  # Centrat horitzontal i verticalment
        button1 = Button(text='Soc boto 1', 
                      color=(1, 1, 1, 1),  # Color blanc
                      font_size='30sp',
                      size_hint=(0.3, 0.1),
                      pos=(0, 2),
                      background_color=("gray"))
        return button1

if __name__ == '__main__':
    MainApp().run()



# Definicio contenidor 01
#class Cont_01(BoxLayout):
#    None

''''
# Definici´o MainApp
class MainApp(App):
    title = "Titol 1"     
    def build(self):
        return Cont_01()


'''    
            
'''
###########################################################
## Funcions universals d'autenticació i consulta a l'API ##
###########################################################

def get_sign(api_secret, payload):
    signature = hmac.new(api_secret.encode("utf-8"), payload.encode("utf-8"), digestmod=sha256).hexdigest()
    print("sign=" + signature)
    return signature

def send_request(method, path, urlpa, payload):
    url = "%s%s?%s&signature=%s" % (APIURL, path, urlpa, get_sign(SECRETKEY, urlpa))
    print(url)
    headers = {
        'X-BX-APIKEY': APIKEY,
    }
    response = requests.request(method, url, headers=headers, data=payload)
    return response.text

def praseParam(paramsMap):
    sortedKeys = sorted(paramsMap)
    paramsStr = "&".join(["%s=%s" % (x, paramsMap[x]) for x in sortedKeys])
    return paramsStr+"&timestamp="+str(int(time.time() * 1000))

#def global_consts():
#with open('berb-api-auth.json', 'r') as file:
#    data = json.load(file)
#    api_key = data["api_key"]
#    secret_key = data["secret_key"]
#    return api_key, secret_key
#    api_key, secret_key = global_consts()

#print("La var api_key val: " + api_key)
#print("La var api_key secret: " + secret_key)

#APIURL = "https://open-api.bingx.com";
#APIKEY = api_key
#SECRETKEY = secret_key    
        
'''    