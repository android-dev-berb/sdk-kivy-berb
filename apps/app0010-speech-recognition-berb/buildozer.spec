[app]
	title = Speech Recognition
	package.name = speech.recognition
	#android.entrypoint = org.kivy.android.PythonActivity
	#android.activity_class_name = org.kivy.android.PythonActivity
	#package.domain = org.test
	package.domain = cat.ticv.asrtest
	source.dir = .
	source.include_exts = py,png,jpg,kv,atlas
	#source.exclude_dirs = tests, bin, venv
	version = 0.0.1
	requirements = kivy,plyer
	#requirements = python3,kivy,pillow
	#presplash.filename = %(source.dir)s/logo.png
	#icon.filename = %(source.dir)s/logo.png
	orientation = portrait
	#author = © Berbascum
	#osx.python_version = 3
	osx.kivy_version = 2.2.1
	fullscreen = 1
	android.archs = arm64-v8a
	#android.allow_backup = True
	#android.api = 30
	#android.minapi = 26
	#android.ndk = 25b
	  ## android.ndk_api usually match android.minapi:
	#android.ndk_api = 26
	#android.sdk_path = /opt/data/0-Linux/01-berb-develop/berb-android-tools/dev-tools-android/sdk-android
	#android.ndk_path = /opt/data/0-Linux/01-berb-develop/berb-android-tools/dev-tools-android/sdk-android/android-ndk-r25b
	#android.ant_path =
	android.permissions = INTERNET,RECORD_AUDIO


	#android.accept_sdk_license = True
	#android.skip_update = False
	  ## (bool) Use --private data storage (True)
	  ## or --dir public storage (False)
	#android.private_storage = True
	# (list) Permissions
	  # https://python-for-android.readthedocs.io/en\
	  # /latest/buildoptions/#build-options-1
	#android.permissions = android.permission.INTERNET, (name=android.permission.WRITE_EXTERNAL_STORAGE;maxSdkVersion=18)#android.features = android.hardware.usb.host

	[buildozer]
	log_level = 2
	warn_on_root = 1
	#build_dir = /opt/data/0-Linux/01-berb-develop/berb-android-tools/dev-tools-android/sdk-kivy/buildozer_build_dir
	#bin_dir = /opt/data/0-Linux/01-berb-develop/berb-android-tools/dev-tools-android/sdk-kivy/buildozer_apk_dir
