## Imports globals
import os

# Imports Kivy
import kivy
kivy.require('2.2.1') # replace with your current kivy version !

from kivy.app import App
from kivy.uix.label import Label


#from kivy.config import Config
#Config.set("graphics", "width", 400)
#Config.set("graphics", "height", 400)
#Eliminar circulo rojo en kivy:
#Config.set('input', 'mouse', 'mouse,multitouch_on_demand') #Para desactivar
#Config.set('input', 'mouse', 'mouse') #Para activar

#Desactivar pantalla de configuraciones:
#def open_settings(*args):
#    pass


class MainApp(App):
    def build(self):
        dir_path = os.getcwd()
        label = Label(text=f"Directori actual:\n{dir_path}",
                      color=(1, 1, 1, 1),  # Color blanc
                      font_size='20sp',  # Mida de la font
                      halign='center', valign='middle') 
        return label

if __name__ == '__main__':
    MainApp().run()
    
