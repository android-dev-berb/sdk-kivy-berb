#!/bin/bash

APK_BUILD_MODE="debug"

echo && echo "Apps disponibles a crear Android apk:"
echo
arr_APP_DIRS=( $(ls apps | grep "app0") )
comptador="0"
for app_dir in ${arr_APP_DIRS[@]}; do
	echo "${comptador} - ${app_dir}"
	let comptador++
done
echo && read -p "Escribiu un nombre d'app: " resposta
app_2_build=${arr_APP_DIRS["${resposta}"]}

echo && echo "Heu seleccionat l'app \"${app_2_build}\""

## Obtenir package name i num de versió de l'apk del buildozer.specs
APK_PAC_NAME=$(grep ^$'\t''package.name = ' \
	apps/${app_2_build}/buildozer.spec \
	| awk '{print $3}')
APK_VERSION=$(grep ^$'\t''version = ' \
	apps/${app_2_build}/buildozer.spec \
	| awk '{print $3}')

[ -z "${APK_PAC_NAME}" ] && echo && echo "Ha fallat el get de la versió de l'apk" && exit
echo && echo "El ṕackage name de l'apk és \"${APK_PAC_NAME}\""
[ -z "${APK_VERSION}" ] && echo && echo "Ha fallat el get de la versió de l'apk" && exit
echo && echo "La versió de l'apk és \"${APK_VERSION}\""
read -p "Intro per a continuar..."

## Inicia python venv
source venv-kivy/bin/activate 

## Build apk
cd "apps/${app_2_build}"
#vim buildozer.spec
buildozer -v android debug 

echo && echo "Arxiu apk de l'app \"${APK_PAC_NAME}\"\"  versió \" ${APK_VERSION} \" copiat a \" apps/${APK_PAC_NAME}/bin \""

## Desactivar python venv
deactivate

echo

